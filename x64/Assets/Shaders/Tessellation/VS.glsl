#version 450 core

layout (location = 0) in vec3 InPosition;
layout (location = 1) in vec3 InNormal;
layout (location = 2) in vec3 InTangent;
layout (location = 3) in vec3 InBitangent;
layout (location = 4) in vec2 InUV;


out vec3 vs_Position;
out vec3 vs_Normal;
out vec3 vs_Tangent;
out vec3 vs_Bitangent;
out vec2 vs_UV;


void main()
{
	vs_Position 	= InPosition;
	vs_Normal 		= InNormal;
	vs_Tangent 		= InTangent;
	vs_Bitangent 	= InBitangent;
	vs_UV 			= InUV * vec2(2.0f, -2.0f);
}