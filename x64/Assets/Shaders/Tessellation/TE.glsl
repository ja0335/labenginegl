#version 450 core

// tell PG to emit triangles in counter-clockwise order with equal spacing
layout(triangles, equal_spacing, ccw) in;

uniform mat4 World;
uniform mat4 View;
uniform mat4 Projection;

uniform sampler2D ESTex0; // Displacement
uniform float DisplacementAmount = 1.0f;
uniform float Time;
uniform vec3 LightDir;

in vec3 cs_Position[];
in vec3 cs_Normal[];
in vec3 cs_Tangent[];
in vec3 cs_Bitangent[];
in vec2 cs_UV[];


out TE_OUT
{
	vec3 EyePosition;
	vec3 EyeDir;
	
	vec3 LightDir;
	vec3 Normal;
	vec2 UV;
} Out;

vec3 Interpolate3DCoord(vec3 v0, vec3 v1, vec3 v2)
{
	return gl_TessCoord.x * v0 + gl_TessCoord.y * v1 + gl_TessCoord.z * v2;
}

vec2 Interpolate2DCoord(vec2 v0, vec2 v1, vec2 v2)
{
	return gl_TessCoord.x * v0 + gl_TessCoord.y * v1 + gl_TessCoord.z * v2;
}

void main()
{	
	mat4 WVP = Projection * View * World;

	vec3 P = Interpolate3DCoord(cs_Position[0], 	cs_Position[1], 	cs_Position[2]);
	vec3 T = Interpolate3DCoord(cs_Tangent[0], 		cs_Tangent[1], 		cs_Tangent[2]);
	vec3 B = Interpolate3DCoord(cs_Bitangent[0], 	cs_Bitangent[1], 	cs_Bitangent[2]);
	vec3 N = Interpolate3DCoord(cs_Normal[0], 		cs_Normal[1], 		cs_Normal[2]);
	Out.UV = Interpolate2DCoord(cs_UV[0],			cs_UV[1],			cs_UV[2]);
	
	float Displacement = texture(ESTex0, Out.UV).r;
	P += N * Displacement * DisplacementAmount;	
	gl_Position = WVP * vec4(P, 1.0f);
	
	T = normalize((World * vec4(T, 0.0f)).xyz);
	B = normalize((World * vec4(B, 0.0f)).xyz);
	N = normalize((World * vec4(N, 0.0f)).xyz);
	
	mat3 TBN = transpose(mat3(T, B, N));
	Out.Normal = normalize(TBN * N);
	
	Out.LightDir = normalize(TBN * LightDir);
	
	Out.EyePosition = vec3(View[0][2], View[1][2], View[2][2]);
	Out.EyeDir = vec3(View[3][0], View[3][1], View[3][2]);
}