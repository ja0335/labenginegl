#version 450 core

// define the number of Vertices in the output patch
// (can be different from the input patch size)
layout (vertices = 3) out;

in vec3 vs_Position[];
in vec3 vs_Normal[];
in vec3 vs_Tangent[];
in vec3 vs_Bitangent[];
in vec2 vs_UV[];

out vec3 cs_Position[];
out vec3 cs_Normal[];
out vec3 cs_Tangent[];
out vec3 cs_Bitangent[];
out vec2 cs_UV[];

uniform float TessLevelInner = 1.0f; 
uniform float TessLevelOuter = 1.0f; 

void main()
{	
	cs_Position[gl_InvocationID] 	= vs_Position[gl_InvocationID];
	cs_Normal[gl_InvocationID] 		= vs_Normal[gl_InvocationID];
	cs_Tangent[gl_InvocationID] 	= vs_Tangent[gl_InvocationID];
	cs_Bitangent[gl_InvocationID] 	= vs_Bitangent[gl_InvocationID];
	cs_UV[gl_InvocationID] 			= vs_UV[gl_InvocationID];
	
	// Calculate the tessellation levels
	if (gl_InvocationID == 0) 
	{
		gl_TessLevelOuter[0] = TessLevelOuter;
		gl_TessLevelOuter[1] = TessLevelOuter;
		gl_TessLevelOuter[2] = TessLevelOuter;
		
		gl_TessLevelInner[0] = TessLevelInner;
	}
}