#version 450 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in TE_OUT
{
	vec3 EyePosition;
	vec3 EyeDir;
	
	vec3 LightDir;
	vec3 Normal;
	vec2 UV;
} In[];

out GS_OUT
{
	vec3 EyePosition;
	vec3 EyeDir;
	
	vec3 LightDir;
	vec3 Normal;
	vec2 UV;
} Out;

void PassThrough(uint Index)
{
	gl_Position 	= gl_in[Index].gl_Position;
	Out.EyePosition = In[Index].EyePosition;
	Out.EyeDir 		= In[Index].EyeDir;
	Out.LightDir 	= In[Index].LightDir;
	Out.Normal 		= In[Index].Normal;
	Out.UV 			= In[Index].UV;
	EmitVertex();
}

void main()
{
	PassThrough(0);
	PassThrough(1);
	PassThrough(2);	
	
	EndPrimitive();
}