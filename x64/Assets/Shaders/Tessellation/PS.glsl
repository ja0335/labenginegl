#version 450 core

in GS_OUT
{
	vec3 EyePosition;
	vec3 EyeDir;
	
	vec3 LightDir;
	vec3 Normal;
	vec2 UV;
} In;

out vec4 Color;

uniform bool bUseColorMap = true;
uniform bool bUseNormalMap = true;
uniform bool bUseLighting = true;
uniform float NearPlane = 0.1f;
uniform float FarPlane = 1000.0f;
uniform sampler2D PSTex0;
uniform sampler2D PSTex1;

void main()
{	
	vec4 Albedo = bUseColorMap ? texture(PSTex0, In.UV) : vec4(1);
	vec3 Normal = bUseNormalMap ?  normalize(2.0f * texture(PSTex1, In.UV).xyz - 1.0f) : In.Normal;
	float Lighting = bUseLighting ? clamp(dot(Normal, -In.LightDir), 0.0f, 1.0f) : 1.0f;
	
	float z = 2.0f * gl_FragCoord.z - 1.0f;
	// Linearize z
	z = (2.0f * NearPlane * FarPlane) / (FarPlane + NearPlane - z * (FarPlane - NearPlane));
	Color = vec4(z / FarPlane);
	
	Color = Albedo * Lighting;
}  