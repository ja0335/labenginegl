#version 450 core

layout (location = 0) in vec3 InPosition;
layout (location = 1) in vec3 InNormal;
layout (location = 2) in vec3 InTangent;
layout (location = 3) in vec3 InBitangent;
layout (location = 4) in vec2 InUV;

uniform vec3 LightDir;

uniform mat4 World;
uniform mat4 View;
uniform mat4 Projection;


out VS_OUT
{
	vec3 EyePosition;
	vec3 EyeDir;
	
	vec3 LightDir;
	vec3 Normal;
	vec2 UV;
} Out;


void main()
{
	mat4 WVP = Projection * View * World;
		
	gl_Position = WVP * vec4(InPosition, 1.0f);
	
	Out.EyePosition = vec3(View[0][2], View[1][2], View[2][2]);
	Out.EyeDir = vec3(View[3][0], View[3][1], View[3][2]);
	
	vec3 T = normalize((World * vec4(InTangent, 0.0f)).xyz);
	vec3 B = normalize((World * vec4(InBitangent, 0.0f)).xyz);
	vec3 N = normalize((World * vec4(InNormal, 0.0f)).xyz);
	
	mat3 TBN = transpose(mat3(T, B, N));
	
	Out.LightDir = normalize(TBN * LightDir);
	Out.Normal = normalize(TBN * N);
	Out.UV = InUV;
}