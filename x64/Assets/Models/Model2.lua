require "Quaternion"

Model  = {
    Mesh = "Assets/Meshes/uvsphere.OBJ", 
    Transform = {
		Rotation = { X = 0.0, Y = 0.0, Z = 0.0, W = 1.0 },
        Position = { X = 100.0, Y = 0.0, Z = 250.0 }, 
		Scale = { X = 1.0, Y = 1.0, Z = 1.0 }
    },
	Material = {
		bUseColorMap = 1,
		bUseNormalMap = 0,
		bUseLighting = 1,
		
		PoligonMode = "Fill", -- Fill, Line, Point
		VertexShader = { 
			File = "Assets/Shaders/VS.glsl"
		},
		PixelShader = {
			File = "Assets/Shaders/PS.glsl",			
			Textures = {
				{
					File = "Assets/Textures/UVChecker.tga",
					Filter = "Anisotropy"
				}
			}
		}
	},
		
	OnInit = function(self)
		SetShaderParameter("bUseColorMap", self.Material.bUseColorMap);
		SetShaderParameter("bUseNormalMap", self.Material.bUseNormalMap);
		SetShaderParameter("bUseLighting", self.Material.bUseLighting);
	end,
	
	OnRender = function(self)		
		
		--self.Transform.Rotation = QuatAxisAngle({ X = 0.0, Y = 1.0, Z = 0.0 }, Time);
		-- self.Transform.Position.X = 200.0;
		-- self.Transform.Position.X = -math.sin(Time) * 100.0;
		-- self.Transform.Position.Y = math.cos(Time) * 100.0;
		
	end,
	
	
	OnKeyDown = function(self, key)	
	end,
}

