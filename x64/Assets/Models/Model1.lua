require "Quaternion"

Model  = {
    Mesh = "Assets/Meshes/uvsphere.OBJ", 
    Transform = {
		Rotation = { X = 0.0, Y = 0.0, Z = 0.0, W = 1.0 },
        Position = { X = 0.0, Y = 0.0, Z = 0.0 }, 
		Scale = { X = 1.0, Y = 1.0, Z = 1.0 }
    },
	Material = {
		TessLevelInner = 10.0,
		TessLevelOuter = 10.0,
		DisplacementAmount = 2.5,
		bUseColorMap = 1,
		bUseNormalMap = 1,
		bUseLighting = 1,
		
		PoligonMode = "Fill", -- Fill, Line, Point
		VertexShader = { 
			File = "Assets/Shaders/Tessellation/VS.glsl"
		},
		TessellationControlShader = { 
			File = "Assets/Shaders/Tessellation/TC.glsl"
		},
		TessellationEvaluationShader = {
			File = "Assets/Shaders/Tessellation/TE.glsl",			
			Textures = {
				{
					File = "Assets/Textures/Brick03_disp.tga",
					Filter = "Anisotropy"
				}	
			}
		},
		GeometryShader = { 
			File = "Assets/Shaders/Tessellation/GS.glsl"
		},
		PixelShader = {
			File = "Assets/Shaders/Tessellation/PS.glsl",			
			Textures = {
				{
					File = "Assets/Textures/Brick03_col.tga",
					Filter = "Anisotropy"
				},	
				{
					File = "Assets/Textures/Brick03_nrm.tga",
					Filter = "Anisotropy"
				}
			}
		}
	},
		
	OnInit = function(self)
		SetShaderParameter("TessLevelInner", self.Material.TessLevelInner);
		SetShaderParameter("TessLevelOuter", self.Material.TessLevelOuter);
		SetShaderParameter("DisplacementAmount", self.Material.DisplacementAmount);
		SetShaderParameter("bUseColorMap", self.Material.bUseColorMap);
		SetShaderParameter("bUseNormalMap", self.Material.bUseNormalMap);
		SetShaderParameter("bUseLighting", self.Material.bUseLighting);
	end,
	
	OnRender = function(self)		
		
		--self.Transform.Rotation = QuatAxisAngle({ X = 0.0, Y = 1.0, Z = 0.0 }, Time);
		-- self.Transform.Position.X = 200.0;
		-- self.Transform.Position.X = -math.sin(Time) * 100.0;
		-- self.Transform.Position.Y = math.cos(Time) * 100.0;
		
	end,
	
	
	OnKeyDown = function(self, key)	
		if key == "Up" then			
			self.Material.DisplacementAmount = self.Material.DisplacementAmount + 0.1;
			SetShaderParameter("DisplacementAmount", self.Material.DisplacementAmount);
		elseif key == "Down" then					
			self.Material.DisplacementAmount = math.max(0, self.Material.DisplacementAmount - 0.1);	
			SetShaderParameter("DisplacementAmount", self.Material.DisplacementAmount);
		end
	end,
}

