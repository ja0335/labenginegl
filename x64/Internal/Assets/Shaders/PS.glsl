#version 450 core

in vec2 UV;

out vec4 Color;

uniform sampler2D ColorTxt;
uniform sampler2D DepthTxt;

uniform vec2 TexelSize;
uniform float NearPlane = 0.1f;
uniform float FarPlane = 2000.0f;

float Gaussian(int x, int y, int KernelWidth)
{
	float SigmaSq = (KernelWidth*KernelWidth) / 16.0f;
	return exp( -(x*x + y*y)/(2*SigmaSq) ) / (2.0f*3.14159265358979f*SigmaSq);
}

void main()
{ 
	int MinKernelWidth = 3;
	int MaxKernelWidth = 45;

	float z = 2.0f * texture(DepthTxt, UV).r - 1.0f;
	// Linearize z
	z = (2.0f * NearPlane * FarPlane) / (FarPlane + NearPlane - z * (FarPlane - NearPlane));
	z /= FarPlane;

	int KernelWidth = int(z * (MaxKernelWidth - MinKernelWidth)) + MinKernelWidth;
	Color = vec4(0.0f);
		
	int x = 0;
	for (int s = -KernelWidth / 2; s <= KernelWidth / 2; ++s)
	{
		int y = 0;

		for (int t = -KernelWidth / 2; t <= KernelWidth / 2; ++t)
		{
			vec2 Offset = vec2(UV.x + s * TexelSize.x, UV.y + t * TexelSize.y );
			int KernelIdx = y * KernelWidth + x;
			Color += texture(ColorTxt, Offset) * Gaussian(s, t, KernelWidth);

			++y;
		}

		++x;
	}
	
	Color.a = 1.0f;

	// Color = texture(ColorTxt, UV) * (pow(1, 0) - 1);
    // Color += vec4(z);	
}