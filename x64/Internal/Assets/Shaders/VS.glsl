#version 450 core

layout (location = 0) in vec2 InPosition;
layout (location = 1) in vec2 InUV;

out vec2 UV;

void main()
{
	gl_Position = vec4(InPosition.x, InPosition.y, 0.0f, 1.0f);
	UV = InUV;
}