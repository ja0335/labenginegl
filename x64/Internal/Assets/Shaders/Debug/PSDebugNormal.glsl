#version 450 core

in GS_OUT
{
	vec4 Color;
} In;

out vec4 Color;

void main()
{
    Color = In.Color;
}  