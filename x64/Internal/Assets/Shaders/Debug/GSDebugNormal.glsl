#version 450 core

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

in VS_OUT
{
	vec3 Normal;
	vec3 Tangent;
} In[];

out GS_OUT
{
	vec4 Color;
} Out;

const float MAGNITUDE = 5.0f;

void GenerateLine(int Index, vec3 Direction, vec4 Color)
{
    gl_Position = gl_in[Index].gl_Position;
	Out.Color = Color;
    EmitVertex();
	
    gl_Position = gl_in[Index].gl_Position + vec4(normalize(Direction), 0.0) * MAGNITUDE;
	Out.Color = Color;
    EmitVertex();
	
    EndPrimitive();
}

void main()
{
	// 1st vertex
    GenerateLine(0, In[0].Normal, vec4(0,0,1,1));
    // GenerateLine(0, In[0].Tangent, vec4(0,1,0,1));
    // GenerateLine(0, cross(In[0].Normal, In[0].Tangent), vec4(1,0,0,1));
	
	// 2nd vertex
    GenerateLine(1, In[1].Normal, vec4(0,0,1,1));
    // GenerateLine(1, In[1].Tangent, vec4(0,1,0,1));
    // GenerateLine(1, cross(In[1].Normal, In[1].Tangent), vec4(1,0,0,1));
	
	// 3th vertex
    GenerateLine(2, In[2].Normal, vec4(0,0,1,1));
    // GenerateLine(2, In[2].Tangent, vec4(0,1,0,1));
    // GenerateLine(2, cross(In[2].Normal, In[2].Tangent), vec4(1,0,0,1));	
}  