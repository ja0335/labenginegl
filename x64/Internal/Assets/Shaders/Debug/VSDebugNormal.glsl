#version 450 core

layout (location = 0) in vec3 InPosition;
layout (location = 1) in vec3 InNormal;
layout (location = 2) in vec3 InTangent;
// layout (location = 3) in vec3 InBitangent;
// layout (location = 4) in vec2 InUV;

uniform mat4 World;
uniform mat4 View;
uniform mat4 Projection;


out VS_OUT
{
	vec3 Normal;
	vec3 Tangent;
} Out;


void main()
{
	mat4 WVP = Projection * View * World;
		
	gl_Position = WVP * vec4(InPosition, 1.0f);
	
	mat3 NMat = mat3(transpose(inverse(View * World)));
	Out.Normal = normalize(vec3(Projection * vec4(NMat * InNormal, 0.0f)));
	
	mat3 TMat = mat3(transpose(inverse(View * World)));
	Out.Tangent = normalize(vec3(Projection * vec4(TMat * InTangent, 0.0f)));
}