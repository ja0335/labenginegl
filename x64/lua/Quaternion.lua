function QuatAxisAngle(Axis, Angle)
	local SinThetaHalf = math.sin(Angle*0.5)
	local CosThetaHalf = math.cos(Angle*0.5)
	local Q = { 
		X = Axis.X * SinThetaHalf, 
		Y = Axis.Y * SinThetaHalf, 
		Z = Axis.Z * SinThetaHalf, 
		W = CosThetaHalf }
	
	return Q
end

function QuatConjugate(Q)
	return { 
		X = -Q.X, 
		Y = -Q.Y, 
		Z = -Q.Z, 
		W = Q.W }
end

function QuatMult(Q1, Q2)
	local Q = {
		X = (Q2.W * Q1.X) + (Q2.X * Q1.W) + (Q2.Y * Q1.Z) - (Q2.Z * Q1.Y),
		Y = (Q2.W * Q1.Y) - (Q2.X * Q1.Z) + (Q2.Y * Q1.W) + (Q2.Z * Q1.X),
		Z = (Q2.W * Q1.Z) + (Q2.X * Q1.Y) - (Q2.Y * Q1.X) + (Q2.Z * Q1.W),
		W = (Q2.W * Q1.W) - (Q2.X * Q1.X) - (Q2.Y * Q1.Y) - (Q2.Z * Q1.Z) }
	
	return Q
end