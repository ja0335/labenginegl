#include "CSceneManager.h"

using namespace std;

CSceneManager::CSceneManager()
{
}


CSceneManager::~CSceneManager()
{
	ShutDown();
}

bool CSceneManager::Initialize(const std::string& SceneFile)
{
	Lua = new LuaTable(SceneFile);

	if (Lua->Open("Models"))
	{
		const uint32_t Length = Lua->Length();

		for (uint32_t i = 1; i <= Length; ++i)
		{
			string ModelFile;
			Lua->Get(i, ModelFile);

			shared_ptr<CModel> Model(new CModel(ModelFile));

			if (Model->Initialize())
			{
				Models.push_back(move(Model));
			}
			else
			{
				cerr << "Could not initize Model " << ModelFile << endl;
			}
		}
	}
	else
	{
		cerr << "ERROR: Provided scene file " << SceneFile << " doesn't have correct structure." << endl;
	}

	Lua->Close();

	return true;
}

void CSceneManager::ShutDown()
{
	if (Lua)
	{
		Lua->Close();
		delete Lua;
		Lua = nullptr;
	}
	
	for (auto &Model : Models)
	{
		Model->ShutDown();
	}

	Models.clear();
}

bool CSceneManager::Render()
{
	for(auto &Model : Models)
	{
		Model->Render();
	}

	return true;
}

void CSceneManager::OnKeyDown(const SDL_KeyboardEvent& KeyboardEvent)
{
	for (auto &Model : Models)
	{
		Model->OnKeyDown(KeyboardEvent);
	}
}

void CSceneManager::OnKeyUp(const SDL_KeyboardEvent& KeyboardEvent)
{
	for (auto &Model : Models)
	{
		Model->OnKeyUp(KeyboardEvent);
	}
}
