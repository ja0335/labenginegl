#pragma once

#include "CGraphics.h"
#include "CSceneManager.h"

#include "luaadapter/LuaTable.hpp"

#include <GL\glew.h>
#include <SDL.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#include <string>
#include <memory>
#include "CInput.h"
#include "CCamera.h"

class CSystem
{
public:
	CSystem();

	~CSystem();
	
	static CSystem * GetSingleton() { return Singleton; }
	int32_t GetScreenWidth() const { return ScreenWidth; }
	int32_t GetScreenHeight() const { return ScreenHeight; }

	const std::unique_ptr<CCamera> & GetCamera() const { return Camera; }

	bool Initialize(const std::string & SceneFile);

	void ShutDown();

	void Run();

	void OnEvent(SDL_Event* Event);

private:
	bool InitializeWindow();
	
	void ShutdownWindow();

	bool Render();
	
private:
	LuaTable		* Lua;
	SDL_Window		* Window;
	static CSystem * Singleton;

	std::unique_ptr<CSceneManager> SceneManager;
	std::unique_ptr<CGraphics> Graphics;
	std::unique_ptr<CCamera> Camera;
	std::unique_ptr<CInput> Input;

	SDL_GLContext GLContext;
	int32_t ScreenWidth;
	int32_t ScreenHeight;
	uint8_t bRender : 1;
	uint8_t bFullScreen : 1;
	std::string ApplicationName;


	uint32_t				ShaderProgram;
	uint32_t				ElementBufferObject;
	uint32_t				VertexArrayObject;
	uint32_t				VertexBufferObject;
};

