#pragma once

#include <glm/glm.hpp>
#include<glm/gtc/quaternion.hpp>

struct SVector3;

struct SQuaternion
{
public:
	SQuaternion();

	SQuaternion(float InX, float InY, float InZ, float InW);

	SQuaternion(SVector3 RotationAxis, float Angle);

	SVector3 RotateVector(const SVector3 & V) const;
	
	SVector3 Up() const;

	SVector3 Right() const;

	SVector3 Forward() const;

	SQuaternion Conjugate() const;

	SQuaternion operator * (const SVector3 & V) const;

	SQuaternion operator * (const SQuaternion & Q) const;

	SQuaternion operator *= (const SQuaternion & Q);
	
	// Begin type Casting operator
	operator SVector3() const;
		
public:
	union
	{
		struct { float X, Y, Z, W; };
		glm::quat _glm_;
	};
	static const SQuaternion Identity;
};

