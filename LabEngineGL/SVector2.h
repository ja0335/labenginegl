#pragma once

#include <glm/glm.hpp>

struct SVector2
{
public:

	SVector2();

	SVector2(float Value);

	SVector2(float InX, float InY);

	void Normalize();

	static SVector2 Normalize(const SVector2 &V);

	SVector2 operator - (const SVector2 & V) const;

	SVector2 operator * (float V) const;
	
public:
	union
	{
		struct { float X, Y; };
		glm::vec2 _glm_;
	};
};