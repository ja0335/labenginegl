#include "CSystem.h"

#include <cstdlib>
#include <memory>

using namespace std;

int main(int argc, const char **argv)
{
	int i = 0;

	for (i = 0; i < argc; ++i)
	{
		if (strcmp(argv[i], "-scene") == 0)
		{
			if (i+1 < argc)
			{
				++i;
				break;
			}

			cerr << "No Scene file provided" << endl;

			return EXIT_FAILURE;
		}
	}

	// Create the system object.
	std::unique_ptr<CSystem> System = std::make_unique<CSystem>();

	if (!System)
		return EXIT_FAILURE;

	// Initialize and run the system object.
	if (System->Initialize(argv[i]))
	{
		System->Run();
	}
	
	// Shutdown and release the system object.
	System.reset();

	return EXIT_SUCCESS;
}
