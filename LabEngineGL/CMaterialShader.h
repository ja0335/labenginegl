#pragma once

#include "SModelDescriptor.h"

class LuaTable;

class CMaterialShader
{
public:
	struct STargaHeader
	{
		uint8_t Data1[12];
		uint16_t Width;
		uint16_t Height;
		uint8_t bpp;
		uint8_t Data2;
	};

	struct STexture
	{
		STargaHeader TargaHeader;
		uint8_t * PixelData = nullptr;

		~STexture()
		{
			delete[] PixelData;
			PixelData = nullptr;
		}
	};

public:
	CMaterialShader();

	~CMaterialShader();

	uint32_t GetShaderProgram() const { return ShaderProgram; }

	bool Initialize(LuaTable *Lua, SMaterialDescriptor& MaterialDescriptor);

	bool Initialize(const SMaterialDescriptor& MaterialDescriptor);

	bool Render(const glm::mat4 & World, const glm::mat4 & View, const glm::mat4 & Projection);

	bool PostRender(uint32_t IndicesSize, const SMaterialDescriptor& MaterialDescriptor);
	
	static bool InitVS(const std::string & ShaderPath, uint32_t & VertexShader);

	static bool InitPS(const std::string & ShaderPath, uint32_t & PixelShader);

private:

	bool FillShaderDescriptor(LuaTable *Lua, SShader& ShaderDescriptor, const std::string & ShaderName);
	
	bool InitCS(const std::string & ShaderPath, uint32_t & ControlShader);

	bool InitES(const std::string & ShaderPath, uint32_t & EvaluationShader);

	bool InitGS(const std::string & ShaderPath, uint32_t & GeometryShader);

	bool InitTexture(std::vector<uint32_t>&	TexturesContainner, const STextureDescriptor & TextureDescriptor);

	bool LoadTarga(const std::string& TexturePath, STexture & Texture);

	ETextureFilterType GetTextureFilterType(const std::string& StrValue) const;

	ETextureWrapType GetTextureWrapType(const std::string& StrValue) const;

	/**
	 * Sets texture sampler in the currently binded texture
	 */
	void SetTextureSampler(const ETextureFilterType &TextureFilterType);
	
private:
	SVector3				LightDir;
	uint32_t				ShaderProgram;
	std::vector<uint32_t>	ESTextures;
	std::vector<uint32_t>	PSTextures;
	bool bIsTessellationShader = false;
};

