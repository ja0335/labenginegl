#include "SQuaternion.h"
#include "SVector3.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>


const SQuaternion SQuaternion::Identity = SQuaternion();

SQuaternion::SQuaternion() :
	X(0), Y(0), Z(0), W(1)
{
}

SQuaternion::SQuaternion(float InX, float InY, float InZ, float InW) :
	X(InX), Y(InY), Z(InZ), W(InW)
{
}

SQuaternion::SQuaternion(SVector3 RotationAxis, float Angle)
{
	_glm_ = glm::angleAxis(Angle, RotationAxis._glm_);
}

SVector3 SQuaternion::RotateVector(const SVector3& V) const
{
	return (*this * V) * Conjugate();
}

SVector3 SQuaternion::Up() const
{
	return RotateVector(SVector3::Up);
}

SVector3 SQuaternion::Right() const
{
	return RotateVector(SVector3::Right);
}

SVector3 SQuaternion::Forward() const
{
	return RotateVector(SVector3::Forward);;
}

SQuaternion SQuaternion::Conjugate() const
{
	const glm::quat Q = glm::conjugate(_glm_);
	return{ Q.x, Q.y, Q.z, Q.w };
}

SQuaternion SQuaternion::operator*(const SVector3 & V) const
{
	const glm::quat Q = _glm_ * glm::quat(0.0f, V._glm_.x, V._glm_.y, V._glm_.z);
	
	return { Q.x, Q.y, Q.z, Q.w };
}

SQuaternion SQuaternion::operator*(const SQuaternion& Q) const
{
	const glm::quat Out = _glm_ * Q._glm_;

	return{ Out.x, Out.y, Out.z, Out.w };
}

SQuaternion SQuaternion::operator*=(const SQuaternion& Q)
{
	_glm_ *= Q._glm_;

	return *this;
}

SQuaternion::operator SVector3() const
{
	return {X, Y, Z};
}