#pragma once
#include <string>
#include <vector>
#include "SVector2.h"
#include "SVector3.h"

class CUtil
{
public:
	struct VertexType
	{
		SVector3 Position;
		SVector3 Normal;
		SVector3 Tangent;
		SVector3 Bitangent;
		SVector2 UV;
	};

public:
	static bool LoadModel(
		const std::string & ModelFilePath,
		std::vector<VertexType> & Vertices,
		std::vector<uint32_t> & Indices);

	static bool GetFileContents(const std::string & ModelFilePath, std::string & FileContents);
};

