#pragma once

#include <glm/glm.hpp>

struct SVector2;
struct SQuaternion;

struct SVector3
{
public:
	SVector3();
	
	SVector3(float Value);
	
	SVector3(float InX, float InY, float InZ);
	
	void Normalize();

	SVector3 operator * (float S) const;

	SVector3 operator * (const SQuaternion & Q) const;

	SVector3 operator + (const SVector3 & V) const;

	SVector3 operator - (const SVector3 & V) const;

	SVector3 operator += (const SVector3 & V);

	SVector3 operator += (const SVector2 & V);
		
public:
	union
	{
		struct { float X, Y, Z; };
		glm::vec3 _glm_;
	};

	static const SVector3 Zero;
	static const SVector3 Up;
	static const SVector3 Right;
	static const SVector3 Forward;
};

