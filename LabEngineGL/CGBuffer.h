#pragma once
#include <cstdint>

class CGBuffer
{
public:

	CGBuffer();

	~CGBuffer();

	bool Initialize();

	void PreRender();

	void PostRender();

private:
	bool InitShaderProgram();

	bool InitFrameBuffer();

	bool InitScreenQuad();
	
private:
	uint32_t FrameBufferObject;
	uint32_t ColorBuffer;
	uint32_t DepthBuffer;
	uint32_t RenderBufferObject;
	uint32_t ScreenQuadVertexArrayObject;
	uint32_t ScreenQuadVertexBufferObject;
	uint32_t ShaderProgram;
};

