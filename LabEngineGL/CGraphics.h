#pragma once
#include "CGBuffer.h"
#include <memory>

class CGraphics
{
public:
	CGraphics();

	~CGraphics();

	bool Initialize();

	void PreRender();

	void PostRender();
private:
	std::unique_ptr<CGBuffer> GBuffer;
};

