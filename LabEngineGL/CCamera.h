#pragma once

#include "SModelDescriptor.h"


class CCamera
{
public:
	CCamera();

	~CCamera();

	float GetMoveSpeed() const { return MoveSpeed; }

	float GetNearPlane() const { return NearPlane; }

	float GetFarPlane() const { return FarPlane; }

	void SetMoveSpeed(float NewMoveSpeed) { MoveSpeed = NewMoveSpeed; }

	void SetPosition(float X, float Y, float Z);
	
	void GetViewMatrix(glm::mat4& OutViewMatrix) const  { OutViewMatrix = ViewMatrix; }
	
	void Update();

protected:
	glm::mat4 ViewMatrix;
	STransforms Transform;
	float MoveSpeed;
	float NearPlane;
	float FarPlane;
};