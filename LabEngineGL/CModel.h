#pragma once

#include <string>
#include "SModelDescriptor.h"
#include "luaadapter/LuaTable.hpp"
#include <memory>
#include "CMaterialShader.h"
#include "CMaterialShader.h"
#include <SDL.h>
#include "luaadapter/LuaFunction.hpp"

class CModel
{
public:
	CModel(const std::string & InModelFile);

	~CModel();

	const SModelDescriptor & GetModelDescriptor() const { return ModelDescriptor; }

	uint32_t GetIndicesSize() const { return IndicesSize; }

	bool Initialize();

	void ShutDown();

	bool Render();

	bool LoadModel(const std::string & ModelFile);

	void OnKeyDown(const SDL_KeyboardEvent &KeyboardEvent);

	void OnKeyUp(const SDL_KeyboardEvent &KeyboardEvent);

private:
	void SetTransformsFromScript();


private:
	LuaTable				* Lua;
	LuaFunction				* Function;

	std::unique_ptr<CMaterialShader> MaterialShader;
	std::unique_ptr<CMaterialShader> DebugShader;

	uint32_t				ElementBufferObject;
	uint32_t				VertexArrayObject;
	uint32_t				VertexBufferObject;

	uint32_t				IndicesSize;

	SModelDescriptor		ModelDescriptor;
	std::string				ModelFile;
};
