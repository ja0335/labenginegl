#include "CGBuffer.h"
#include <GL/glew.h>
#include "CSystem.h"

using namespace std;

CGBuffer::CGBuffer()
{
}


CGBuffer::~CGBuffer()
{
}

bool CGBuffer::Initialize()
{
	InitShaderProgram();
	InitScreenQuad();
	InitFrameBuffer();

	return true;
}

void CGBuffer::PreRender()
{
	glBindFramebuffer(GL_FRAMEBUFFER, FrameBufferObject);
	// reenable depth test as in previous PostRender it was disabled
	glEnable(GL_DEPTH_TEST);
}

void CGBuffer::PostRender()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	// disable depth test so screen-space quad isn't discarded due to depth test.
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// clear all relevant buffers
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(ShaderProgram);
	glBindVertexArray(ScreenQuadVertexArrayObject);

	// use the color attachment texture as the texture of the quad plane
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ColorBuffer);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, DepthBuffer);

	const uint32_t TexelSize = glGetUniformLocation(ShaderProgram, "TexelSize");
	const uint32_t NearPlane = glGetUniformLocation(ShaderProgram, "NearPlane");
	const uint32_t FarPlane = glGetUniformLocation(ShaderProgram, "FarPlane");

	glUniform2f(TexelSize, 1.0f / CSystem::GetSingleton()->GetScreenWidth(), 1.0f / CSystem::GetSingleton()->GetScreenHeight());
	glUniform1f(NearPlane, CSystem::GetSingleton()->GetCamera()->GetNearPlane());
	glUniform1f(FarPlane, CSystem::GetSingleton()->GetCamera()->GetFarPlane());

	glDrawArrays(GL_TRIANGLES, 0, 6);
}

bool CGBuffer::InitShaderProgram()
{
	uint32_t VertexShader = 0;
	uint32_t PixelShader = 0;
	
	if (!CMaterialShader::InitVS("Internal/Assets/Shaders/VS.glsl", VertexShader))
		return false;

	if (!CMaterialShader::InitPS("Internal/Assets/Shaders/PS.glsl", PixelShader))
		return false;

	ShaderProgram = glCreateProgram();
	glAttachShader(ShaderProgram, VertexShader);
	glAttachShader(ShaderProgram, PixelShader);
	glLinkProgram(ShaderProgram);

	int  Success;
	char InfoLog[512];
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

	if (!Success)
	{
		glGetProgramInfoLog(ShaderProgram, 512, nullptr, InfoLog);

		cerr << "Error linking the Shader Program for GBuffer" << endl;
		cerr << InfoLog << std::endl;

		return false;
	}

	glDeleteShader(VertexShader);
	glDeleteShader(PixelShader);

	return true;
}

bool CGBuffer::InitFrameBuffer()
{
	glGenFramebuffers(1, &FrameBufferObject);
	glBindFramebuffer(GL_FRAMEBUFFER, FrameBufferObject);
	{
		glGenTextures(1, &ColorBuffer);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, ColorBuffer);
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
				CSystem::GetSingleton()->GetScreenWidth(),
				CSystem::GetSingleton()->GetScreenHeight(),
				0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ColorBuffer, 0);
		}

		glUseProgram(ShaderProgram);
		const uint32_t ColorLocation = glGetUniformLocation(ShaderProgram, "ColorTxt");
		glUniform1i(ColorLocation, 0);
		
		glGenTextures(1, &DepthBuffer);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, DepthBuffer);
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24,
				CSystem::GetSingleton()->GetScreenWidth(),
				CSystem::GetSingleton()->GetScreenHeight(),
				0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, DepthBuffer, 0);
		}
		const uint32_t DepthLocation = glGetUniformLocation(ShaderProgram, "DepthTxt");
		glUniform1i(DepthLocation, 1);

		// create a renderbuffer object for depth and stencil
		//glGenRenderbuffers(1, &RenderBufferObject);
		//glBindRenderbuffer(GL_RENDERBUFFER, RenderBufferObject);
		//{
		//	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8,
		//		CSystem::GetSingleton()->GetScreenWidth(),
		//		CSystem::GetSingleton()->GetScreenHeight());
		//	glFramebufferRenderbuffer(
		//		GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RenderBufferObject);
		//}
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return true;
}

bool CGBuffer::InitScreenQuad()
{
	float quadVertices[] = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
		// positions   // texCoords
		-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		1.0f, -1.0f,  1.0f, 0.0f,

		-1.0f,  1.0f,  0.0f, 1.0f,
		1.0f, -1.0f,  1.0f, 0.0f,
		1.0f,  1.0f,  1.0f, 1.0f
	};

	glGenVertexArrays(1, &ScreenQuadVertexArrayObject);
	glGenBuffers(1, &ScreenQuadVertexBufferObject);

	glBindVertexArray(ScreenQuadVertexArrayObject);
	{
		glBindBuffer(GL_ARRAY_BUFFER, ScreenQuadVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);

			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);

			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
		}
	}

	return true;
}
