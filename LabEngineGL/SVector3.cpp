#include "SVector3.h"
#include "SVector2.h"
#include "SQuaternion.h"

const SVector3 SVector3::Zero = SVector3(0.0f);
const SVector3 SVector3::Up = SVector3(0.0f, 1.0f, 0.0f);
const SVector3 SVector3::Right = SVector3(1.0f, 0.0f, 0.0f);
const SVector3 SVector3::Forward = SVector3(0.0f, 0.0f, 1.0f);

SVector3::SVector3() :
	SVector3(0.0f)
{
}

SVector3::SVector3(float Value) :
	SVector3(Value, Value, Value)
{
}

SVector3::SVector3(float InX, float InY, float InZ) :
	X(InX), Y(InY), Z(InZ)
{
}

void SVector3::Normalize()
{
	glm::normalize(_glm_);
}

SVector3 SVector3::operator*(float S) const
{
	SVector3 V;
	V._glm_ = _glm_ * S;

	return V;
}

SVector3 SVector3::operator*(const SQuaternion& Q) const
{
	const glm::vec3 V = _glm_ * Q._glm_;

	return{ V.x, V.y, V.z };
}

SVector3 SVector3::operator+(const SVector3& V) const
{
	SVector3 Out;
	Out._glm_ = _glm_ + V._glm_;

	return Out;
}

SVector3 SVector3::operator-(const SVector3& V) const
{
	SVector3 Out;
	Out._glm_ = _glm_ - V._glm_;

	return Out;
}

SVector3 SVector3::operator+=(const SVector3& V)
{
	*this = *this + V;

	return *this;
}

SVector3 SVector3::operator+=(const SVector2& V)
{
	*this += {V.X, V.Y, 0.0f};

	return *this;
}