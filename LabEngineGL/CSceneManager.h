#pragma once

#include <string>
#include <vector>
#include "CModel.h"
#include "luaadapter/LuaTable.hpp"
#include <memory>
#include <SDL.h>

class CSceneManager
{
public:
	CSceneManager();

	~CSceneManager();

	bool Initialize(const std::string & SceneFile);

	void ShutDown();

	bool Render();

	void OnKeyDown(const SDL_KeyboardEvent &KeyboardEvent);

	void OnKeyUp(const SDL_KeyboardEvent &KeyboardEvent);

private:
	LuaTable		* Lua;

	std::vector<std::shared_ptr<CModel>> Models;
};

