#pragma once

#include "SVector2.h"
#include "SVector3.h"
#include <cstdint>
#include <SDL.h>


class CInput
{
public:
	CInput();

	~CInput();

	static CInput * GetSingleton() { return Singleton; }

	bool IsKeyDown(SDL_Scancode Key) const;
	
	bool IsMouseLeftButtonDown() const { return bIsMouseLeftButtonDown; }
	bool IsMouseMiddleButtonDown() const { return bIsMouseMiddleButtonDown; }
	bool IsMouseRightButtonDown() const { return bIsMouseRightButtonDown; }

	const SVector2 & GetMousePosition() const { return MousePosition; }

	const SVector2 & GetMouseDelta() const { return MouseDelta; }
	
	const SVector3 & GetInputVector() const { return InputVector; }

	void Initialize();

	bool Update();

private:
	static CInput * Singleton;

	SVector2 ScreenDimensions;
	SVector2 MousePosition;
	SVector2 OldMousePosition;
	SVector2 MouseDelta;
	SVector3 InputVector;

	uint8_t bIsMouseLeftButtonDown : 1;
	uint8_t bIsMouseMiddleButtonDown : 1;
	uint8_t bIsMouseRightButtonDown : 1;
};

