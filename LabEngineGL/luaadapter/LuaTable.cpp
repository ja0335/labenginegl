#ifndef LUA_TABLE_H
#include "LuaTable.hpp"
#endif
LuaTable::LuaTable(const std::string &filename)
	: luaAdapter{ new LuaAdapter(filename) }, Lua{ luaAdapter->GetLuaState() }, print{ luaAdapter->IsDebugging() }
{
}

LuaTable::LuaTable(LuaAdapter &lua)
    : Lua{lua.GetLuaState()}, print{lua.IsDebugging()}, luaAdapter(nullptr){}

LuaTable::LuaTable(lua_State *const lua) : Lua{lua}, print{false}, luaAdapter(nullptr) {}

LuaTable::~LuaTable()
{
	if (luaAdapter != nullptr)
		delete luaAdapter;
}

bool LuaTable::Get(const char *name) {
  if ((!this->Lua) || (!name) || (!lua_istable(this->Lua, -1)))
    return false;

  lua_getfield(this->Lua, -1, name);
  return true;
}

bool LuaTable::Get(const char *name, std::string &result) {
  if (this->Get(name) == false)
    return false;

  if (lua_type(this->Lua, -1) != LUA_TSTRING) {
    lua_pop(this->Lua, 1);
    return false;
  }
  result = lua_tostring(this->Lua, -1);
  if (this->print)
    std::cout << "\t" << LUA_PREFIX << "get string-field '" << name << "' = '"
              << result << "' \n";
  lua_pop(this->Lua, 1);
  return true;
}

bool LuaTable::Get(const char* name, bool& result)
{
	if (this->Get(name) == false)
		return false;
	if (lua_isboolean(this->Lua, -1) != 1) {
		lua_pop(this->Lua, 1);
		return false;
	}
	result = lua_toboolean(this->Lua, -1);
	if (this->print)
		std::cout << "\t" << LUA_PREFIX << "get bool-field '" << name << "' = '"
		<< result << "' \n";
	lua_pop(this->Lua, 1);
	return true;
}

bool LuaTable::Open(const char *name) {
  if (!name)
    return false;

  if (lua_istable(this->Lua, -1)) {
    return this->OpenNested(name);
  }
  lua_getglobal(this->Lua, name); // void

  if (lua_istable(this->Lua, -1)) {

    if (this->print)
      std::cout << LUA_PREFIX << "open table '" << name << "' \n";
    return true;
  }
  lua_pop(this->Lua, 1);
  return false;
}

bool LuaTable::Open(unsigned i)
{
	if (this->GetI(i) == false)
		return false;

	if (lua_istable(this->Lua, -1)) {

		if (this->print)
			std::cout << LUA_PREFIX << "open table '" << i << "' \n";
		return true;
	}
	lua_pop(this->Lua, 1);
	return false;
}

unsigned short int LuaTable::Length() {
  unsigned short int result{0};
  if ((!this->Lua)) {
    return result;
  }
  if (lua_istable(this->Lua, -1) == false) {
    return result;
  }
  lua_len(this->Lua, -1);
  result = lua_tointeger(this->Lua, -1);
  lua_pop(this->Lua, 1);
  return result;
}

bool LuaTable::Get(const char *name, int &result) {
  if (this->Get(name) == false)
    return false;
  if (lua_isnumber(this->Lua, -1) != 1) {
    lua_pop(this->Lua, 1);
    return false;
  }
  result = lua_tointeger(this->Lua, -1);
  if (this->print)
    std::cout << "\t" << LUA_PREFIX << "get int-field '" << name << "' = '"
              << result << "' \n";
  lua_pop(this->Lua, 1);
  return true;
}

bool LuaTable::Get(const char* name, unsigned char& result){
	if (this->Get(name) == false)
		return false;
	if (lua_isnumber(this->Lua, -1) != 1) {
		lua_pop(this->Lua, 1);
		return false;
	}
	result = lua_tointeger(this->Lua, -1);
	if (this->print)
		std::cout << "\t" << LUA_PREFIX << "get (unsigned char)-field '" << name << "' = '" << result << "' \n";
	lua_pop(this->Lua, 1);
	return true;
}

bool LuaTable::Get(const char* name, float &result) {
	if (this->Get(name) == false)
		return false;
	if (lua_isnumber(this->Lua, -1) != 1) {
		lua_pop(this->Lua, 1);
		return false;
	}
	result = lua_tonumber(this->Lua, -1);
	if (this->print)
		std::cout << "\t" << LUA_PREFIX << "get int-field '" << name << "' = '"
		<< result << "' \n";
	lua_pop(this->Lua, 1);
	return true;
}

bool LuaTable::Set(const char* name, void* pointerUserData)
{
	if ((!this->Lua) || (!name) || (!lua_istable(this->Lua, -1)))
		return false;

	lua_pushlightuserdata(this->Lua, pointerUserData);
	lua_setfield(this->Lua, -2, name);
	return true;
}

bool LuaTable::Set(const char* name, float value)
{
	if ((!this->Lua) || (!name) || (!lua_istable(this->Lua, -1)))
		return false;

	lua_pushnumber(this->Lua, value);
	lua_setfield(this->Lua, -2, name);
	return true;
}

bool LuaTable::Set(const char* name, const char* value)
{
	if ((!this->Lua) || (!name) || (!lua_istable(this->Lua, -1)))
		return false;

	lua_pushstring(this->Lua, value);
	lua_setfield(this->Lua, -2, name);
	return true;
}

bool LuaTable::Get(unsigned short int i, int &result) {
  if (this->GetI(i) == false)
    return false;

  if (lua_isnumber(this->Lua, -1) != 1) {
    lua_pop(this->Lua, 1);
    return false;
  }
  result = lua_tointeger(this->Lua, -1);
  if (this->print)
    std::cout << "\t" << LUA_PREFIX << "get int-field " << i << " = '" << result
              << "' \n";
  lua_pop(this->Lua, 1);
  return true;
}

bool LuaTable::Get(unsigned short int i, double &result) {
  if (this->GetI(i) == false)
    return false;

  if (lua_isnumber(this->Lua, -1) != 1) {
    lua_pop(this->Lua, 1);
    return false;
  }
  result = lua_tonumber(this->Lua, -1);
  if (this->print)
    std::cout << "\t" << LUA_PREFIX << "get double-field " << i << " = '"
              << result << "' \n";
  lua_pop(this->Lua, 1);
  return true;
}

bool LuaTable::Get(unsigned short int i, float &result) {
  if (this->GetI(i) == false)
    return false;

  if (lua_isnumber(this->Lua, -1) != 1) {
    lua_pop(this->Lua, 1);
    return false;
  }
  result = (float)lua_tonumber(this->Lua, -1);
  if (this->print)
    std::cout << "\t" << LUA_PREFIX << "get float-field " << i << " = '"
              << result << "' \n";
  lua_pop(this->Lua, 1);
  return true;
}

bool LuaTable::Get(unsigned short int i, std::string &result) {
  if (this->GetI(i) == false)
    return false;
  if (lua_type(this->Lua, -1) != LUA_TSTRING) {
    lua_pop(this->Lua, 1);
    return false;
  }
  result = lua_tostring(this->Lua, -1);
  if (this->print)
    std::cout << "\t" << LUA_PREFIX << "get string-field " << i << " = '"
              << result << "' \n";
  lua_pop(this->Lua, 1);
  return true;
}

bool LuaTable::Get(unsigned short int j, unsigned short int i, double &result) {
  if (this->GetI(j) == false) {
    return false;
  }
  if (this->GetI(i) == false) {
    lua_pop(this->Lua, 1);
    return false;
  }
  if (lua_isnumber(this->Lua, -1))
    result = lua_tonumber(this->Lua, -1);
  if (this->print)
    std::cout << "\t"
              << "\t" << LUA_PREFIX << "get nested double-field (" << j << "|"
              << i << ")' = '" << result << "' \n";
  lua_pop(this->Lua, 2);
  return true;
}

bool LuaTable::Get(unsigned short int j, unsigned short int i, float &result) {
  if (this->GetI(j) == false) {
    return false;
  }
  if (this->GetI(i) == false) {
    lua_pop(this->Lua, 1);
    return false;
  }
  if (lua_isnumber(this->Lua, -1))
    result = (float)lua_tonumber(this->Lua, -1);
  if (this->print)
    std::cout << "\t"
              << "\t" << LUA_PREFIX << "get nested float-field (" << j << "|"
              << i << ")' = '" << result << "' \n";
  lua_pop(this->Lua, 2);
  return true;
}

bool LuaTable::Get(unsigned short int j, unsigned short int i, int &result) {
  if (this->GetI(j) == false) {
    return false;
  }
  if (this->GetI(i) == false) {
    lua_pop(this->Lua, 1);
    return false;
  }
  if (lua_isnumber(this->Lua, -1))
    result = lua_tointeger(this->Lua, -1);
  if (this->print)
    std::cout << "\t"
              << "\t" << LUA_PREFIX << "get nested int-field (" << j << "|" << i
              << ")' = '" << result << "' \n";
  lua_pop(this->Lua, 2);
  return true;
}

bool LuaTable::Get(unsigned short int j, unsigned short int i,
                   std::string &result) {
  if (this->GetI(j) == false) {
    return false;
  }
  if (this->GetI(i) == false) {
    lua_pop(this->Lua, 1);
    return false;
  }
  if (lua_isstring(this->Lua, -1))
    result = lua_tostring(this->Lua, -1);
  if (this->print)
    std::cout << "\t"
              << "\t" << LUA_PREFIX << "get nested string-field (" << j << "|"
              << i << ")' = '" << result << "' \n";
  lua_pop(this->Lua, 2);
  return true;
}

bool LuaTable::Get(unsigned short int k, unsigned short int j,
                   unsigned short int i, int &result) {
  if (this->GetI(k) == false) {
    return false;
  }
  if (this->GetI(j) == false) {
    return false;
  }
  if (this->GetI(i) == false) {
    lua_pop(this->Lua, 1);
    return false;
  }
  if (lua_isnumber(this->Lua, -1))
    result = lua_tointeger(this->Lua, -1);
  if (this->print)
    std::cout << "\t"
              << "\t" << LUA_PREFIX << "get nested int-field (" << j << "|" << i
              << ")' = '" << result << "' \n";
  lua_pop(this->Lua, 3);
  return true;
}

bool LuaTable::Push(lua_CFunction function, const char* functionName)
{
	if ((!this->Lua) || (!lua_istable(this->Lua, -1)))
		return false;

	static const luaL_Reg functions[] =
	{
		{ functionName, function },
		{ NULL, NULL }
	};

	luaL_setfuncs(this->Lua, functions, 0);

	return true;
}

bool LuaTable::Call(const char* name)
{
	if (this->Get(name) == false)
		return false;
	
	if (lua_type(this->Lua, -1) != LUA_TFUNCTION) {
		lua_pop(this->Lua, 1);
		return false;
	}

	lua_pushvalue(this->Lua, -2);
	const int call{ lua_pcall(this->Lua, 1, 0, 0) };
	if (call == LUA_OK) {
		return true;
	}
	std::cerr << LUA_PREFIX << "Error: pcall failed. Code: ";
	std::cerr << call;
	std::cerr << ", '" << lua_tostring(this->Lua, -1) << "'\n";
	lua_pop(this->Lua, 1);
	return false;
}

bool LuaTable::Call(const char* name, const std::string arg)
{
	if (this->Get(name) == false)
		return false;

	if (lua_type(this->Lua, -1) != LUA_TFUNCTION) {
		lua_pop(this->Lua, 1);
		return false;
	}

	lua_pushvalue(this->Lua, -2);
	lua_pushlstring(this->Lua, arg.c_str(), arg.length());
	const int call{ lua_pcall(this->Lua, 2, 0, 0) };
	if (call == LUA_OK) {
		return true;
	}
	std::cerr << LUA_PREFIX << "Error: pcall failed. Code: ";
	std::cerr << call;
	std::cerr << ", '" << lua_tostring(this->Lua, -1) << "'\n";
	lua_pop(this->Lua, 1);
	return false;
}

bool LuaTable::Call(const char* functionName, const unsigned short argc, const float args[], int& result)
{
	if (!this->Lua) {
		return false;
	}
	lua_getglobal(this->Lua, functionName);

	for (unsigned char i = 0; i < argc; ++i)
		if (args + i)
			lua_pushnumber(this->Lua, args[i]);

	const int call{ lua_pcall(this->Lua, argc, 1, 0) };
	if (call != LUA_OK) {
		return false;
	}

	if (lua_isnumber(this->Lua, -1)) {
		result = lua_tointeger(this->Lua, -1);
	}
	lua_pop(this->Lua, 1);
	return true;
}

bool LuaTable::GetI(unsigned short int i) {
  if ((!this->Lua) || (!lua_istable(this->Lua, -1)) || (i < 1))
    return false;

  lua_rawgeti(this->Lua, -1, i);
  return true;
}

bool LuaTable::OpenNested(const char *name) {
  if ((!this->Lua)) {
    return false;
  }
  if (this->print)
    std::cout << "\t" << LUA_PREFIX << "opening nested table '" << name
              << "' ... ";

  if (lua_istable(this->Lua, -1) == false) {
    return false;
  }

  lua_getfield(this->Lua, -1, name);

  if (lua_isnil(this->Lua, -1)) {
    lua_pop(this->Lua, 1);
    return false;
  }

  if (lua_istable(this->Lua, -1)) {
    if (this->print)
      std::cout << "ok! \n";
    return true;
  }
  lua_pop(this->Lua, 1);
  return false;
}