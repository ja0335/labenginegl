#include "CSystem.h"

using namespace std;

CSystem * CSystem::Singleton = nullptr;

CSystem::CSystem()
{
	Singleton = this;
	Window = nullptr;
	Lua = nullptr;
	ScreenWidth = 1240;
	ScreenHeight = 720;
	bFullScreen = false;
	bRender = true;
	ApplicationName = "Lab Engine";
}

CSystem::~CSystem()
{
	ShutDown();
}

bool CSystem::Initialize(const std::string & SceneFile)
{
	Lua = new LuaTable("Config/AppConfig.lua");

	if (Lua->Open("AppConfig"))
	{
		Lua->Get("ScreenWidth", ScreenWidth);
		Lua->Get("ScreenHeight", ScreenHeight);

		bool bLuaFullScreen;
		Lua->Get("FullScreen", bLuaFullScreen);
		bFullScreen = bLuaFullScreen;
	}

	Graphics = make_unique<CGraphics>();
	
	Camera = make_unique<CCamera>();
	Camera->SetPosition(0.0f, 0.0f, -250.0f);

	SceneManager = make_unique<CSceneManager>();
	Input = make_unique<CInput>();
		
	InitializeWindow();
	
	Graphics->Initialize();
	
	SceneManager->Initialize(SceneFile);

	Input->Initialize();
	
	return true;
}

void CSystem::ShutDown()
{
	if (Lua)
	{
		Lua->Close();
		delete Lua;
		Lua = nullptr;
	}

	ShutdownWindow();

	Singleton = nullptr;
}

void CSystem::Run()
{
	SDL_Event Event;

	while (bRender)
	{
		while (SDL_PollEvent(&Event))
		{
			OnEvent(&Event);
		}

		if (Event.type == SDL_QUIT || (Event.type == SDL_KEYDOWN && Event.key.keysym.sym == SDLK_ESCAPE))
			bRender = false;
		else
			bRender = Render();

	}
}

void CSystem::OnEvent(SDL_Event* Event)
{
	switch (Event->type)
	{
	case SDL_KEYDOWN:
		SceneManager->OnKeyDown(Event->key);
		break;
	case SDL_KEYUP:
		SceneManager->OnKeyUp(Event->key);
		break;
	}
}

bool CSystem::InitializeWindow()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		return false;
		
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	int contextFlags = 0;
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_FLAGS, &contextFlags);
	contextFlags |= SDL_GL_CONTEXT_DEBUG_FLAG;
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, contextFlags);

	Window = SDL_CreateWindow(ApplicationName.c_str(),
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		ScreenWidth, ScreenHeight,
		(bFullScreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : SDL_WINDOW_RESIZABLE) | SDL_WINDOW_OPENGL);

	// Create OpenGL context
	GLContext = SDL_GL_CreateContext(Window);
	
	if (GLContext == nullptr)
		return false;
	
	return true;
}

void CSystem::ShutdownWindow()
{
	SDL_GL_DeleteContext(GLContext);
	SDL_DestroyWindow(Window);
	SDL_Quit();
	Window = nullptr;
}

bool CSystem::Render()
{
	Input->Update();

	Camera->Update();
	
	Graphics->PreRender();

	SceneManager->Render();

	Graphics->PostRender();

	SDL_GL_SwapWindow(Window);

	return true;
}
