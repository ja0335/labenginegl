#include "SVector2.h"
#include <glm/gtx/norm.inl>

SVector2::SVector2():
	SVector2(0.0f)
{
}

SVector2::SVector2(float Value) :
	SVector2(Value, Value)
{
}

SVector2::SVector2(float InX, float InY) :
	X(InX), Y(InY)
{
}

void SVector2::Normalize()
{
	_glm_ = glm::normalize(_glm_);
}

SVector2 SVector2::Normalize(const SVector2& V)
{
	glm::vec2 Out = {0.0f, 0.0f};
	
	if (glm::length2(V._glm_) > 1e-6f)
		Out =  glm::normalize(V._glm_);

	return{ Out.x, Out.y };
}

SVector2 SVector2::operator-(const SVector2& V) const
{
	return{ X - V.X, Y - V.Y };
}

SVector2 SVector2::operator*(float V) const
{
	return{ X * V, Y * V };
}