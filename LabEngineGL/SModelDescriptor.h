#pragma once

#include <string>
#include <vector>
#include "SQuaternion.h"
#include "SVector3.h"
#include <iostream>
#include <GL/glew.h>

struct SColor
{
	uint8_t R = 255;
	uint8_t G = 255;
	uint8_t B = 255;
	uint8_t A = 255;
};

enum class ETextureType : uint8_t
{
	R8,
	R8G8B8,
	R8G8B8A8,
};

/*
 * In case of separate filtering Min_Mag will
 * be applied respectively. i.e.
 * if TextureFilterType=Nearest_Linear then
 *		Min=Nearest
 *		Mag=Linear
 */
enum class ETextureFilterType : uint8_t
{
	Nearest_Linear,
	Nearest_Nearest,
	Anisotropy,
	None,
};

enum class ETextureWrapType : uint32_t
{
	Repeat = GL_REPEAT,
	MirrorredRepeat = GL_MIRRORED_REPEAT,
	ClampToEdge = GL_CLAMP_TO_EDGE,
	ClampToBorder = GL_CLAMP_TO_BORDER,
};

struct STextureDescriptor
{
	std::string File;
	ETextureType Type;
	ETextureFilterType Filter;
	ETextureWrapType UWrap;
	ETextureWrapType VWrap;
	SColor BorderColor;
};

struct SShader
{
	std::string File;
	std::vector<STextureDescriptor> Textures;
};

struct SMaterialDescriptor
{
	uint32_t PoligonMode;
	SShader VertexShader;
	SShader TessellationControlShader;
	SShader TessellationEvaluationShader;
	SShader GeometryShader;
	SShader PixelShader;
};

struct STransforms
{
	SQuaternion Rotation;
	SVector3 Position;
	SVector3 Scale;
};

struct SModelDescriptor
{
	std::string Mesh;
	STransforms Transforms;
	SMaterialDescriptor Material;
};