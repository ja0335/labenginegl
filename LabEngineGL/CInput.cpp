#include "CInput.h"

#include <iostream>
#include <cassert>
#include <SDL.h>
#include "CSystem.h"

CInput * CInput::Singleton = nullptr;

CInput::CInput()
{
	assert(Singleton == nullptr);
	Singleton = this;

	MouseDelta = { 0, 0 };
	MousePosition = { 0, 0 };
	bIsMouseLeftButtonDown = false;
	bIsMouseMiddleButtonDown = false;
	bIsMouseRightButtonDown = false;
}

CInput::~CInput()
{
}


void CInput::Initialize()
{
	ScreenDimensions.X = float(CSystem::GetSingleton()->GetScreenWidth());
	ScreenDimensions.Y = float(CSystem::GetSingleton()->GetScreenHeight());
}

bool CInput::Update()
{
	SDL_PumpEvents();
	Sint32 MouseX, MouseY;
	const Uint32 MouseState = SDL_GetMouseState(&MouseX, &MouseY);

	bIsMouseLeftButtonDown = false;
	bIsMouseMiddleButtonDown = false;
	bIsMouseRightButtonDown = false;

	if (MouseState & SDL_BUTTON(SDL_BUTTON_LEFT))
		bIsMouseLeftButtonDown = true;
	if (MouseState & SDL_BUTTON(SDL_BUTTON_MIDDLE))
		bIsMouseMiddleButtonDown = true;
	if (MouseState & SDL_BUTTON(SDL_BUTTON_RIGHT))
		bIsMouseRightButtonDown = true;
	
	OldMousePosition = MousePosition;

	MousePosition.X = MouseX / float(ScreenDimensions.X);
	MousePosition.Y = MouseY / float(ScreenDimensions.Y);
	
	MouseDelta = MousePosition - OldMousePosition;

	InputVector = { 0, 0, 0 };


	const Uint8 * KeyboardState = SDL_GetKeyboardState(nullptr);

	if (KeyboardState[SDL_SCANCODE_ESCAPE])
		return false;

	if (KeyboardState[SDL_SCANCODE_D])
		InputVector.X = 1.0f;
	else if (KeyboardState[SDL_SCANCODE_A])
		InputVector.X = -1.0f;

	if (KeyboardState[SDL_SCANCODE_Q])
		InputVector.Y = -1.0f;
	else if (KeyboardState[SDL_SCANCODE_E])
		InputVector.Y = 1.0f;

	if (KeyboardState[SDL_SCANCODE_W])
		InputVector.Z = 1.0f;
	else if (KeyboardState[SDL_SCANCODE_S])
		InputVector.Z = -1.0f;

	return true;
}

bool CInput::IsKeyDown(SDL_Scancode Key) const
{
	if (Key >= SDL_NUM_SCANCODES)
		return false;

	SDL_PumpEvents();

	const Uint8 * KeyboardState = SDL_GetKeyboardState(nullptr);

	if (KeyboardState[Key])
		return true;

	return false;
}
