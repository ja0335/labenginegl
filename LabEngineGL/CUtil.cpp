#include "CUtil.h"

#include <fstream>
#include <iostream>

using namespace std;

void SplitString(const string &Str, const string & Delimiter, vector<string> &OutResult)
{
	char * pch = strtok(const_cast<char*>(Str.c_str()), Delimiter.c_str());

	while (pch != nullptr)
	{
		OutResult.emplace_back(pch);
		pch = strtok(nullptr, Delimiter.c_str());
	}
}

void GetVertexData(const string & LineData, int32_t &v, int32_t &n, int32_t &uv)
{
	vector<string> FaceData;
	SplitString(LineData, "/", FaceData);
	if (FaceData.size() == 3)
	{
		v = strtol(FaceData[0].c_str(), nullptr, 10) - 1;
		uv = strtol(FaceData[1].c_str(), nullptr, 10) - 1;
		n = strtol(FaceData[2].c_str(), nullptr, 10) - 1;
	}
}

bool CUtil::LoadModel(const std::string& ModelFilePath, std::vector<VertexType>& Vertices, std::vector<uint32_t>& Indices)
{
	cout << "Loading model " << ModelFilePath << endl;

	vector<SVector3> v;
	vector<SVector3> n;
	vector<SVector2> uv;

	ifstream ModelFile(ModelFilePath);

	if (ModelFile.is_open())
	{
		string Line;
		vector<string> LineData;
		vector<string> FaceData;

		while (getline(ModelFile, Line))
		{
			SplitString(Line, " ", LineData);

			if (LineData.empty())
				continue;

			if (LineData[0] == "v" && LineData.size() == 4)
			{
				SVector3 Vertex(
					strtof(LineData[1].c_str(), nullptr),
					strtof(LineData[2].c_str(), nullptr),
					strtof(LineData[3].c_str(), nullptr));

				v.emplace_back(Vertex);
			}
			else if (LineData[0] == "vn" && LineData.size() == 4)
			{
				SVector3 VertexNormal(
					strtof(LineData[1].c_str(), nullptr),
					strtof(LineData[2].c_str(), nullptr),
					strtof(LineData[3].c_str(), nullptr));

				n.emplace_back(VertexNormal);
			}
			else if (LineData[0] == "vt" && LineData.size() == 3)
			{
				SVector2 VertexTexture(
					strtof(LineData[1].c_str(), nullptr),
					1 - strtof(LineData[2].c_str(), nullptr));

				uv.emplace_back(VertexTexture);
			}
			else if (LineData[0] == "f" && LineData.size() == 4)
			{
				int32_t v0, n0, uv0;
				int32_t v1, n1, uv1;
				int32_t v2, n2, uv2;

				GetVertexData(LineData[1], v0, n0, uv0);
				GetVertexData(LineData[3], v1, n1, uv1);
				GetVertexData(LineData[2], v2, n2, uv2);

				const SVector3 edge1 = SVector3(v[v1].X, v[v1].Y, v[v1].Z) - SVector3(v[v0].X, v[v0].Y, v[v0].Z);
				const SVector3 edge2 = SVector3(v[v2].X, v[v2].Y, v[v2].Z) - SVector3(v[v0].X, v[v0].Y, v[v0].Z);
				const SVector3 deltaUV1 = SVector3(uv[uv1].X, uv[uv1].Y, 0.0f) - SVector3(uv[uv0].X, uv[uv0].Y, 0.0f);
				const SVector3 deltaUV2 = SVector3(uv[uv2].X, uv[uv2].Y, 0.0f) - SVector3(uv[uv0].X, uv[uv0].Y, 0.0f);

				const float f = 1.0f / (deltaUV1.X * deltaUV2.Y - deltaUV2.X * deltaUV1.Y);

				SVector3 tangent;
				tangent.X = f * (deltaUV2.Y * edge1.X - deltaUV1.Y * edge2.X);
				tangent.Y = f * (deltaUV2.Y * edge1.Y - deltaUV1.Y * edge2.Y);
				tangent.Z = f * (deltaUV2.Y * edge1.Z - deltaUV1.Y * edge2.Z);
				tangent.Normalize();

				SVector3 bitangent;
				bitangent.X = f * (-deltaUV2.X * edge1.X + deltaUV1.X * edge2.X);
				bitangent.Y = f * (-deltaUV2.X * edge1.Y + deltaUV1.X * edge2.Y);
				bitangent.Z = f * (-deltaUV2.X * edge1.Z + deltaUV1.X * edge2.Z);
				bitangent.Normalize();

				const VertexType vt_0 = { v[v0] , n[n0], tangent, bitangent, uv[uv0] };
				Vertices.emplace_back(vt_0);
				Indices.emplace_back(uint32_t(Vertices.size()) - 1);

				const VertexType vt_1 = { v[v1] , n[n1], tangent, bitangent,  uv[uv1] };
				Vertices.emplace_back(vt_1);
				Indices.emplace_back(uint32_t(Vertices.size()) - 1);

				const VertexType vt_2 = { v[v2], n[n2], tangent, bitangent,  uv[uv2] };
				Vertices.emplace_back(vt_2);
				Indices.emplace_back(uint32_t(Vertices.size()) - 1);
			}

			LineData.clear();
		}

		ModelFile.close();
	}
	else
	{
		return false;
	}

	v.clear();
	n.clear();
	uv.clear();


	cout << "End loading model " << ModelFilePath << endl;

	return true;
}

bool CUtil::GetFileContents(const std::string & ModelFilePath, std::string & FileContents)
{
	ifstream ModelFile(ModelFilePath);

	if (ModelFile.is_open())
	{
		ModelFile.seekg(0, std::ios::end);
		FileContents.reserve(ModelFile.tellg());
		ModelFile.seekg(0, std::ios::beg);

		FileContents.assign((std::istreambuf_iterator<char>(ModelFile)), std::istreambuf_iterator<char>());

		ModelFile.close();

		return true;
	}

	return false;
}
