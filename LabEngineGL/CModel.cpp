#include "CSystem.h"
#include "CModel.h"
#include "CUtil.h"
#include "CGraphics.h"
#include "SVector3.h"
#include "SQuaternion.h"

#include "luaadapter/LuaAdapter.hpp"
#include "luaadapter/LuaFunction.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;

static int IsKeyDown(lua_State *L)
{
	if (!L)
		return 0;

	LuaAdapter lua{ L };
	const Sint64 Key{ lua_tointeger(L, 1) };

	if (CInput::GetSingleton()->IsKeyDown(static_cast<SDL_Scancode>(Key)))
		lua.Push(true);
	else
		lua.Push(false);

	return 1;
}

CModel::CModel(const std::string & InModelFile)
{
	ModelFile = InModelFile;
	Lua = nullptr;
}

CModel::~CModel()
{
	ShutDown();
}

bool CModel::Initialize()
{
	Lua = new LuaTable(ModelFile);

	Function = new LuaFunction( *Lua->GetLuaAdapter() );
	Function->PushGlobalVar("Time", 0.0f);
	Function->Push(IsKeyDown, "IsKeyDown");


	if (Lua->Open("Model"))
	{
		if (!Lua->Get("Mesh", ModelDescriptor.Mesh))
			return false;

		SetTransformsFromScript();
		
		MaterialShader = make_unique<CMaterialShader>();
		if (!MaterialShader->Initialize(Lua, ModelDescriptor.Material))
			return false;
	}
	
	//SMaterialDescriptor DebugMaterialDesc;
	//DebugMaterialDesc.VertexShader.File = "Internal/Assets/Shaders/Debug/VSDebugNormal.glsl";
	//DebugMaterialDesc.GeometryShader.File = "Internal/Assets/Shaders/Debug/GSDebugNormal.glsl";
	//DebugMaterialDesc.PixelShader.File = "Internal/Assets/Shaders/Debug/PSDebugNormal.glsl";;

	//DebugShader = make_unique<CMaterialShader>();
	//if (!DebugShader->Initialize(DebugMaterialDesc))
	//	return false;
	
	if (!LoadModel(ModelDescriptor.Mesh))
		return false;	


	Lua->Call("OnInit");

	return true;
}

void CModel::ShutDown()
{
	if (Lua)
	{
		Lua->Close();
		delete Lua;
		Lua = nullptr;
	}

	if (Function)
	{
		delete Function;
		Function = nullptr;
	}

	glDeleteVertexArrays(1, &VertexArrayObject);
	glDeleteBuffers(1, &VertexBufferObject);
	glDeleteBuffers(1, &ElementBufferObject);
}

bool CModel::Render()
{
	Function->PushGlobalVar("Time", SDL_GetTicks()/1000.0f);
	Lua->Call("OnRender");
	SetTransformsFromScript();

	glm::mat4 World;
	World = glm::translate(World, ModelDescriptor.Transforms.Position._glm_);
	World = glm::scale(World, ModelDescriptor.Transforms.Scale._glm_);
	World = World * glm::toMat4(ModelDescriptor.Transforms.Rotation._glm_);

	glm::mat4 View;
	CSystem::GetSingleton()->GetCamera()->GetViewMatrix(View);

	const float FieldOfView = 3.141592654f / 4.0f;
	const float ScreenAspect = CSystem::GetSingleton()->GetScreenWidth() / float(CSystem::GetSingleton()->GetScreenHeight());
	const glm::mat4 Projection = glm::perspective(
		FieldOfView, 
		ScreenAspect, 
		CSystem::GetSingleton()->GetCamera()->GetNearPlane(), 
		CSystem::GetSingleton()->GetCamera()->GetFarPlane());
	
	MaterialShader->Render(World, View, Projection);
	glBindVertexArray(VertexArrayObject);
	MaterialShader->PostRender(IndicesSize, ModelDescriptor.Material);

	if (DebugShader)
	{
		DebugShader->Render(World, View, Projection);
		glBindVertexArray(VertexArrayObject);
		glDrawElements(GL_TRIANGLES, IndicesSize, GL_UNSIGNED_INT, 0);
	}
	
	glBindVertexArray(0);

	return true;
}

bool CModel::LoadModel(const std::string& ModelFile)
{
	vector<CUtil::VertexType> Vertices;
	vector<uint32_t> Indices;

	if (!CUtil::LoadModel(ModelDescriptor.Mesh, Vertices, Indices))
		return false;

	IndicesSize = Indices.size();
	
	// Create buffers
	glGenVertexArrays(1, &VertexArrayObject);
	glGenBuffers(1, &VertexBufferObject);
	glGenBuffers(1, &ElementBufferObject);

	// Bind VAO
	glBindVertexArray(VertexArrayObject);
	{
		// Bind VBO
		glBindBuffer(GL_ARRAY_BUFFER, VertexBufferObject);
		// Fill VBO
		glBufferData(GL_ARRAY_BUFFER, Vertices.size() * sizeof(CUtil::VertexType), &Vertices[0], GL_STATIC_DRAW);

		// Bind EBO
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ElementBufferObject);
		// Fill EBO
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, Indices.size() * sizeof(uint32_t), &Indices[0], GL_STATIC_DRAW);

		// Enable Vertex Position
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(CUtil::VertexType), (void*)(0));

		// Enable Vertex Position
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(CUtil::VertexType), (void*)(offsetof(CUtil::VertexType, Normal)));
		
		// Enable Vertex Tangent
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(CUtil::VertexType), reinterpret_cast<void*>(offsetof(CUtil::VertexType, Tangent)));

		// Enable Vertex Tangent
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(CUtil::VertexType), reinterpret_cast<void*>(offsetof(CUtil::VertexType, Bitangent)));

		// Enable Vertex UV
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(CUtil::VertexType), reinterpret_cast<void*>(offsetof(CUtil::VertexType, UV)));
		
		// Unbind buffers
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	// Unbind VAO
	glBindVertexArray(0);
		
	return true;
}

void CModel::OnKeyDown(const SDL_KeyboardEvent& KeyboardEvent)
{
	const std::string KeyName = SDL_GetKeyName(KeyboardEvent.keysym.sym);
	Lua->Call("OnKeyDown", KeyName);
}

void CModel::OnKeyUp(const SDL_KeyboardEvent& KeyboardEvent)
{
	const std::string KeyName = SDL_GetKeyName(KeyboardEvent.keysym.sym);
	Lua->Call("OnKeyUp", KeyName);
}

void CModel::SetTransformsFromScript()
{
	if (Lua->Open("Transform"))
	{
		if (Lua->Open("Rotation"))
		{
			Lua->Get("X", ModelDescriptor.Transforms.Rotation.X);
			Lua->Get("Y", ModelDescriptor.Transforms.Rotation.Y);
			Lua->Get("Z", ModelDescriptor.Transforms.Rotation.Z);
			Lua->Get("W", ModelDescriptor.Transforms.Rotation.W);

			Lua->Close();
		}

		if (Lua->Open("Position"))
		{
			Lua->Get("X", ModelDescriptor.Transforms.Position.X);
			Lua->Get("Y", ModelDescriptor.Transforms.Position.Y);
			Lua->Get("Z", ModelDescriptor.Transforms.Position.Z);

			Lua->Close();
		}

		if (Lua->Open("Scale"))
		{
			Lua->Get("X", ModelDescriptor.Transforms.Scale.X);
			Lua->Get("Y", ModelDescriptor.Transforms.Scale.Y);
			Lua->Get("Z", ModelDescriptor.Transforms.Scale.Z);

			Lua->Close();
		}

		Lua->Close();
	}
}