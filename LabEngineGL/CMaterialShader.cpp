#include "CMaterialShader.h"
#include "CSystem.h"
#include "CUtil.h"

#include "luaadapter/LuaAdapter.hpp"
#include "luaadapter/LuaFunction.hpp"

#include <glm/gtc/type_ptr.inl>

using namespace std;

static int SetShaderParameter(lua_State* L)
{
	if (!L)
		return 0;
	lua_getglobal(L, "Object");
	if (CMaterialShader * MaterialShader = static_cast<CMaterialShader*>(lua_touserdata(L, -1)))
	{
		const char* VarName = lua_tostring(L, 1);
		const float Value = static_cast<float>(lua_tonumber(L, 2));
		
		glUseProgram(MaterialShader->GetShaderProgram());
		const uint32_t VarLocation = glGetUniformLocation(MaterialShader->GetShaderProgram(), VarName);
		glUniform1f(VarLocation, Value);
	}

	return 1;
}


CMaterialShader::CMaterialShader()
{
	LightDir = { 0, -1, 0 };
}

CMaterialShader::~CMaterialShader()
{
}

bool CMaterialShader::Initialize(LuaTable *Lua, SMaterialDescriptor& MaterialDescriptor)
{
	if (Lua->Open("Material"))
	{
		string PoligonMode;
		Lua->Get("PoligonMode", PoligonMode);

		MaterialDescriptor.PoligonMode = GL_FILL;

		if (PoligonMode == "Point")
			MaterialDescriptor.PoligonMode = GL_POINT;
		else if (PoligonMode == "Line")
			MaterialDescriptor.PoligonMode = GL_LINE;


		FillShaderDescriptor(Lua, MaterialDescriptor.VertexShader, "VertexShader");
		FillShaderDescriptor(Lua, MaterialDescriptor.TessellationControlShader, "TessellationControlShader");
		FillShaderDescriptor(Lua, MaterialDescriptor.TessellationEvaluationShader, "TessellationEvaluationShader");
		FillShaderDescriptor(Lua, MaterialDescriptor.GeometryShader, "GeometryShader");
		FillShaderDescriptor(Lua, MaterialDescriptor.PixelShader, "PixelShader");

		Lua->Close();
	}
	else
	{
		cerr << "ERROR: Cannot open Lua Material Descriptor." << endl;
		return false;
	}

	return Initialize(MaterialDescriptor);
}

bool CMaterialShader::Initialize(const SMaterialDescriptor& MaterialDescriptor)
{
	string FileContents;
	uint32_t VertexShader = 0;
	uint32_t ControlShader = 0;
	uint32_t EvaluationShader = 0;
	uint32_t GeometryShader = 0;
	uint32_t PixelShader = 0;

	//int32_t MaxPatchVertices;
	//glGetIntegerv(GL_MAX_PATCH_VERTICES, &MaxPatchVertices);

	ShaderProgram = glCreateProgram();

	if (InitVS(MaterialDescriptor.VertexShader.File, VertexShader))
		glAttachShader(ShaderProgram, VertexShader);
	else
		return false;

	if (!MaterialDescriptor.TessellationControlShader.File.empty() && !MaterialDescriptor.TessellationEvaluationShader.File.empty())
	{
		if (InitCS(MaterialDescriptor.TessellationControlShader.File, ControlShader))
			glAttachShader(ShaderProgram, ControlShader);
		else
			return false;

		if (InitES(MaterialDescriptor.TessellationEvaluationShader.File, EvaluationShader))
			glAttachShader(ShaderProgram, EvaluationShader);
		else
			return false;

		bIsTessellationShader = true;
	}

	if (!MaterialDescriptor.GeometryShader.File.empty())
	{
		if (InitGS(MaterialDescriptor.GeometryShader.File, GeometryShader))
			glAttachShader(ShaderProgram, GeometryShader);
		else
			return false;
	}

	if (InitPS(MaterialDescriptor.PixelShader.File, PixelShader))
		glAttachShader(ShaderProgram, PixelShader);
	else
		return false;

	glLinkProgram(ShaderProgram);

	int  Success;
	char InfoLog[512];
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

	if (!Success)
	{
		glGetProgramInfoLog(ShaderProgram, 512, nullptr, InfoLog);

		cerr << "Error linking the Shader Program" << endl;
		cerr << InfoLog << endl;

		return false;
	}

	glDeleteShader(VertexShader);
	if (ControlShader != 0) glDeleteShader(ControlShader);
	if (EvaluationShader != 0) glDeleteShader(EvaluationShader);
	if (GeometryShader != 0) glDeleteShader(GeometryShader);
	glDeleteShader(PixelShader);

	uint32_t TextureIdx = 0;

	// Crete Evaluation Shader samplers
	string TexturueParamName = "ESTex";

	for (int32_t i = 0; i < MaterialDescriptor.TessellationEvaluationShader.Textures.size(); ++i)
	{
		if (InitTexture(ESTextures, MaterialDescriptor.TessellationEvaluationShader.Textures[i]))
		{
			glUseProgram(ShaderProgram);

			const string TextureName = TexturueParamName + to_string(i);
			const uint32_t TextureLocation = glGetUniformLocation(ShaderProgram, TextureName.c_str());
			glUniform1i(TextureLocation, TextureIdx++);
		}
	}

	// Crete Pixel Shader samplers
	TexturueParamName = "PSTex";

	for (int32_t i = 0; i < MaterialDescriptor.PixelShader.Textures.size(); ++i)
	{
		if (InitTexture(PSTextures, MaterialDescriptor.PixelShader.Textures[i]))
		{
			glUseProgram(ShaderProgram);

			const string TextureName = TexturueParamName + to_string(i);
			const uint32_t TextureLocation = glGetUniformLocation(ShaderProgram, TextureName.c_str());
			glUniform1i(TextureLocation, TextureIdx++);
		}
	}

	return true;
}


bool CMaterialShader::Render(const glm::mat4 & World, const glm::mat4 & View, const glm::mat4 & Projection)
{
	const SVector2 &MouseDelta = CInput::GetSingleton()->GetMouseDelta();
	if (CInput::GetSingleton()->IsMouseLeftButtonDown() && CInput::GetSingleton()->IsKeyDown(SDL_SCANCODE_L))
	{
		const SQuaternion Q1 = SQuaternion(SVector3(1, 0, 0), -MouseDelta.Y);
		const SQuaternion Q2 = SQuaternion(SVector3(0, 1, 0), MouseDelta.X);
		const SQuaternion Rot = Q1 * Q2;
		LightDir = Rot * (LightDir * Rot.Conjugate());
	}

	uint32_t TextureIdx = 0;
	for(uint32_t i = 0; i < ESTextures.size(); ++i)
	{
		glActiveTexture(GL_TEXTURE0 + TextureIdx++);
		glBindTexture(GL_TEXTURE_2D, ESTextures[i]);
	}
	for(uint32_t i = 0; i < PSTextures.size(); ++i)
	{
		glActiveTexture(GL_TEXTURE0 + TextureIdx++);
		glBindTexture(GL_TEXTURE_2D, PSTextures[i]);
	}

	glUseProgram(ShaderProgram);

	if (bIsTessellationShader)
		glPatchParameteri(GL_PATCH_VERTICES, 3);

	// Get parameters locations in shaders
	const uint32_t VSWorld = glGetUniformLocation(ShaderProgram, "World");
	const uint32_t VSView = glGetUniformLocation(ShaderProgram, "View");
	const uint32_t VSProjection = glGetUniformLocation(ShaderProgram, "Projection");
	
	const uint32_t Time = glGetUniformLocation(ShaderProgram, "Time");
	const uint32_t NearPlane = glGetUniformLocation(ShaderProgram, "NearPlane");
	const uint32_t FarPlane = glGetUniformLocation(ShaderProgram, "FarPlane");
	const uint32_t LightDirLoc = glGetUniformLocation(ShaderProgram, "LightDir");

	glUniformMatrix4fv(VSWorld, 1, GL_FALSE, glm::value_ptr(World));
	glUniformMatrix4fv(VSView, 1, GL_FALSE, glm::value_ptr(View));
	glUniformMatrix4fv(VSProjection, 1, GL_FALSE, glm::value_ptr(Projection));

	glUniform1f(NearPlane, CSystem::GetSingleton()->GetCamera()->GetNearPlane());
	glUniform1f(FarPlane, CSystem::GetSingleton()->GetCamera()->GetFarPlane());
	glUniform1f(Time, SDL_GetTicks() / 1000.0f);
	glUniform3f(LightDirLoc, LightDir.X, LightDir.Y, LightDir.Z);
	
	return true;
}

bool CMaterialShader::PostRender(uint32_t IndicesSize, const SMaterialDescriptor& MaterialDescriptor)
{
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glPolygonMode(GL_FRONT_AND_BACK, MaterialDescriptor.PoligonMode);

	if (bIsTessellationShader)
		glDrawElements(GL_PATCHES, IndicesSize, GL_UNSIGNED_INT, 0);
	else
		glDrawElements(GL_TRIANGLES, IndicesSize, GL_UNSIGNED_INT, 0);


	return true;
}

bool CMaterialShader::FillShaderDescriptor(LuaTable *Lua, SShader& ShaderDescriptor, const std::string & ShaderName)
{
	LuaFunction luaFunction{ *Lua->GetLuaAdapter() };
	luaFunction.PushGlobalVar("Object", this);
	luaFunction.Push(SetShaderParameter, "SetShaderParameter");

	if (Lua->Open(ShaderName))
	{
		Lua->Get("File", ShaderDescriptor.File);

		if (Lua->Open("Textures"))
		{
			const uint32_t Length = Lua->Length();

			for (uint32_t i = 1; i <= Length; ++i)
			{
				if (Lua->Open(i))
				{
					STextureDescriptor Texture;

					Lua->Get("File", Texture.File);

					string Filter = "Nearest_Linear";
					string UWrap = "Repeat";
					string VWrap = "Repeat";

					Lua->Get("Filter", Filter); 
					Lua->Get("UWrap", UWrap);
					Lua->Get("VWrap", VWrap);

					Texture.Filter = GetTextureFilterType(Filter);
					Texture.UWrap = GetTextureWrapType(UWrap);
					Texture.VWrap = GetTextureWrapType(VWrap);

					if (Texture.UWrap == ETextureWrapType::ClampToBorder ||
						Texture.VWrap == ETextureWrapType::ClampToBorder)
					{
						if (Lua->Open("BorderColor"))
						{
							Lua->Get("R", Texture.BorderColor.R);
							Lua->Get("G", Texture.BorderColor.G);
							Lua->Get("B", Texture.BorderColor.B);
							Lua->Get("A", Texture.BorderColor.A);
							Lua->Close();
						}
					}
					
					ShaderDescriptor.Textures.push_back(move(Texture));

					Lua->Close();
				}
			}

			Lua->Close();
		}

		Lua->Close();
	}

	return true;
}

bool CMaterialShader::InitVS(const std::string & ShaderPath, uint32_t & VertexShader)
{
	string FileContents;

	if (!CUtil::GetFileContents(ShaderPath, FileContents))
	{
		cerr << "ERROR: Could not load Vertex Shader " << ShaderPath << ". Please verify path location. " << endl;

		return false;
	}

	VertexShader = glCreateShader(GL_VERTEX_SHADER);
	const char* ShaderSource = FileContents.c_str();
	glShaderSource(VertexShader, 1, &ShaderSource, nullptr);
	glCompileShader(VertexShader);

	int  Success;
	char InfoLog[512];
	glGetShaderiv(VertexShader, GL_COMPILE_STATUS, &Success);

	if (!Success)
	{
		glGetShaderInfoLog(VertexShader, 512, nullptr, InfoLog);

		cerr << "Error initializing the Vertex Shader" << endl;
		cerr << InfoLog << endl;

		return false;
	}
	
	return true;
}

bool CMaterialShader::InitCS(const std::string& ShaderPath, uint32_t& ControlShader)
{
	string FileContents;

	if (!CUtil::GetFileContents(ShaderPath, FileContents))
	{
		cerr << "ERROR: Could not load Control Shader " << ShaderPath << ". Please verify path location. " << endl;

		return false;
	}

	ControlShader = glCreateShader(GL_TESS_CONTROL_SHADER);
	const char* ShaderSource = FileContents.c_str();
	glShaderSource(ControlShader, 1, &ShaderSource, nullptr);
	glCompileShader(ControlShader);

	int  Success;
	char InfoLog[512];
	glGetShaderiv(ControlShader, GL_COMPILE_STATUS, &Success);

	if (!Success)
	{
		glGetShaderInfoLog(ControlShader, 512, nullptr, InfoLog);

		cerr << "Error initializing the Control Shader" << endl;
		cerr << InfoLog << endl;

		return false;
	}

	return true;
}

bool CMaterialShader::InitES(const std::string& ShaderPath, uint32_t& EvaluationShader)
{
	string FileContents;

	if (!CUtil::GetFileContents(ShaderPath, FileContents))
	{
		cerr << "ERROR: Could not load Evaluation Shader " << ShaderPath << ". Please verify path location. " << endl;

		return false;
	}

	EvaluationShader = glCreateShader(GL_TESS_EVALUATION_SHADER);
	const char* ShaderSource = FileContents.c_str();
	glShaderSource(EvaluationShader, 1, &ShaderSource, nullptr);
	glCompileShader(EvaluationShader);

	int  Success;
	char InfoLog[512];
	glGetShaderiv(EvaluationShader, GL_COMPILE_STATUS, &Success);

	if (!Success)
	{
		glGetShaderInfoLog(EvaluationShader, 512, nullptr, InfoLog);

		cerr << "Error initializing the Evaluation Shader" << endl;
		cerr << InfoLog << endl;

		return false;
	}

	return true;
}

bool CMaterialShader::InitGS(const std::string& ShaderPath, uint32_t& GeometryShader)
{
	string FileContents;

	if (!CUtil::GetFileContents(ShaderPath, FileContents))
	{
		cerr << "ERROR: Could not load Geometry Shader " << ShaderPath << ". Please verify path location. " << endl;

		return false;
	}

	GeometryShader = glCreateShader(GL_GEOMETRY_SHADER);
	const char* ShaderSource = FileContents.c_str();
	glShaderSource(GeometryShader, 1, &ShaderSource, nullptr);
	glCompileShader(GeometryShader);

	int  Success;
	char InfoLog[512];
	glGetShaderiv(GeometryShader, GL_COMPILE_STATUS, &Success);

	if (!Success)
	{
		glGetShaderInfoLog(GeometryShader, 512, nullptr, InfoLog);

		cerr << "Error initializing the Geometry Shader" << endl;
		cerr << InfoLog << endl;

		return false;
	}

	return true;
}

bool CMaterialShader::InitPS(const std::string & ShaderPath, uint32_t & PixelShader)
{
	string FileContents;

	if (!CUtil::GetFileContents(ShaderPath, FileContents))
	{
		cerr << "ERROR: Could not load Pixel Shader " << ShaderPath << ". Please verify path location. " << endl;

		return false;
	}

	PixelShader = glCreateShader(GL_FRAGMENT_SHADER);
	const char* ShaderSource = FileContents.c_str();
	glShaderSource(PixelShader, 1, &ShaderSource, nullptr);
	glCompileShader(PixelShader);

	int  Success;
	char InfoLog[512];
	glGetShaderiv(PixelShader, GL_COMPILE_STATUS, &Success);

	if (!Success)
	{
		glGetShaderInfoLog(PixelShader, 512, nullptr, InfoLog);

		cerr << "Error initializing the Pixel Shader" << endl;
		cerr << InfoLog << endl;

		return false;
	}

	return true;
}

bool CMaterialShader::InitTexture(std::vector<uint32_t>& TexturesContainner, const STextureDescriptor & TextureDescriptor)
{
	STexture Texture;

	if (LoadTarga(TextureDescriptor.File, Texture))
	{
		GLint InternalFormat = GL_RGB;
		GLenum Format = GL_RGB;
		GLenum Type = GL_UNSIGNED_BYTE;

		if (Texture.TargaHeader.bpp == 8)
		{
			InternalFormat = GL_RED;
			Format = GL_RED;
		}
		else if (Texture.TargaHeader.bpp == 24)
		{
			InternalFormat = GL_RGB;
			Format = GL_RGB;
		}
		else if (Texture.TargaHeader.bpp == 32)
		{
			InternalFormat = GL_RGBA;
			Format = GL_RGBA;
		}

		uint32_t TextureID;

		glGenTextures(1, &TextureID);
		glActiveTexture(GL_TEXTURE0 + static_cast<GLenum>(TexturesContainner.size()));
		glBindTexture(GL_TEXTURE_2D, TextureID);
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, static_cast<GLint>(TextureDescriptor.UWrap));
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, static_cast<GLint>(TextureDescriptor.VWrap));

			if (TextureDescriptor.UWrap == ETextureWrapType::ClampToBorder ||
				TextureDescriptor.VWrap == ETextureWrapType::ClampToBorder)
			{
				float BorderColor[] = {
					TextureDescriptor.BorderColor.R / 255.0f,
					TextureDescriptor.BorderColor.G / 255.0f,
					TextureDescriptor.BorderColor.B / 255.0f,
					TextureDescriptor.BorderColor.A / 255.0f
				};
				glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, BorderColor);
			}

			SetTextureSampler(TextureDescriptor.Filter);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, 0.0f);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 0.0f);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, GL_TEXTURE_MAX_LOD);

			glTexImage2D(GL_TEXTURE_2D, 0, InternalFormat, Texture.TargaHeader.Width, Texture.TargaHeader.Height, 0, Format, Type, Texture.PixelData);
			glGenerateMipmap(GL_TEXTURE_2D);
		}

		// Add the created texture to the container
		TexturesContainner.push_back(TextureID);

		return true;
	}


	return false;
}

bool CMaterialShader::LoadTarga(const std::string& TexturePath, STexture & Texture)
{
	FILE* filePtr;
	
	// Open the targa file for reading in binary.
	int error = fopen_s(&filePtr, TexturePath.c_str(), "rb");
	if (error != 0)
	{
		cerr << "ERROR: Could not load Texture " << TexturePath << ". Please verify path location. " << endl;

		return false;
	}

	// Read in the file header.
	uint32_t count = static_cast<uint32_t>(fread(&Texture.TargaHeader, sizeof(STargaHeader), 1, filePtr));
	if (count != 1)
	{
		return false;
	}
	
	// Check that it is 8, 24, 32 bit
	const bool bIsSupported = Texture.TargaHeader.bpp == 8 || Texture.TargaHeader.bpp == 24 || Texture.TargaHeader.bpp == 32;
	if (!bIsSupported)
	{
		cerr << "ERROR: No supported texture format " << static_cast<uint32_t>(Texture.TargaHeader.bpp) << " bits per pixel color " << TexturePath << endl;
		return false;
	}

	const uint8_t BytesPerPixel = Texture.TargaHeader.bpp / 8;

	// Calculate the size of the image data.
	const uint32_t imageSize = Texture.TargaHeader.Width * Texture.TargaHeader.Height * BytesPerPixel;

	// Allocate memory for the targa image data.
	uint8_t * PixelData = new uint8_t[imageSize];
	if (!PixelData)
	{
		return false;
	}

	// Read in the targa image data.
	count = static_cast<uint32_t>(fread(PixelData, 1, imageSize, filePtr));
	if (count != imageSize)
	{
		return false;
	}

	// Close the file.
	error = fclose(filePtr);
	if (error != 0)
	{
		return false;
	}

	// Allocate memory for the targa destination data.
	Texture.PixelData = new uint8_t[imageSize];
	if (!Texture.PixelData)
	{
		return false;
	}

	// Initialize the index into the targa destination data array.
	int32_t index = 0;

	// Initialize the index into the targa image data.
	uint32_t k = (Texture.TargaHeader.Width * Texture.TargaHeader.Height * BytesPerPixel) - (Texture.TargaHeader.Width * BytesPerPixel);

	// Now copy the targa image data into the targa destination array in the correct order since the targa format is stored upside down.
	for (uint32_t j = 0; j < Texture.TargaHeader.Height; ++j)
	{
		for (uint32_t i = 0; i < Texture.TargaHeader.Width; ++i)
		{
			if (BytesPerPixel == 4)
			{
				Texture.PixelData[index + 0] = PixelData[index + 2];
				Texture.PixelData[index + 1] = PixelData[index + 1];
				Texture.PixelData[index + 2] = PixelData[index + 0];
				Texture.PixelData[index + 3] = PixelData[index + 3];
			}
			else if (BytesPerPixel == 3)
			{
				Texture.PixelData[index + 0] = PixelData[index + 2];
				Texture.PixelData[index + 1] = PixelData[index + 1];
				Texture.PixelData[index + 2] = PixelData[index + 0];
			}
			else
			{
				Texture.PixelData[index] = PixelData[index];
			}

			k += BytesPerPixel;
			index += BytesPerPixel;
		}

		// Set the targa image data index back to the preceding row at the beginning of the column since its reading it in upside down.
		k -= (Texture.TargaHeader.Width * 8);
	}

	// Release the targa image data now that it was copied into the destination array.
	delete[] PixelData;
	PixelData = nullptr;

	return true;
}

ETextureFilterType CMaterialShader::GetTextureFilterType(const std::string& StrValue) const
{
	if (StrValue == "Nearest_Linear")
		return ETextureFilterType::Nearest_Linear;
	if (StrValue == "Nearest_Nearest")
		return ETextureFilterType::Nearest_Nearest;
	else if (StrValue == "Anisotropy")
		return ETextureFilterType::Anisotropy;
	
	cerr << "WARNING: Filter type" << StrValue << " is not supported." << endl;

	// return default 
	return ETextureFilterType::Nearest_Linear;
}

ETextureWrapType CMaterialShader::GetTextureWrapType(const std::string& StrValue) const
{
	if (StrValue == "Repeat")
		return ETextureWrapType::Repeat;
	else if (StrValue == "MirrorredRepeat")
		return ETextureWrapType::MirrorredRepeat;
	else if (StrValue == "ClampToEdge")
		return ETextureWrapType::ClampToEdge;
	else if (StrValue == "ClampToBorder")
		return ETextureWrapType::ClampToBorder;

	cerr << "WARNING: Wrap type " << StrValue << " is not supported." << endl;

	// return default 
	return ETextureWrapType::Repeat;
}

void CMaterialShader::SetTextureSampler(const ETextureFilterType &TextureFilterType)
{
	if (TextureFilterType == ETextureFilterType::Anisotropy)
	{
		if (glewIsSupported("GL_EXT_texture_filter_anisotropic") || GLEW_EXT_texture_filter_anisotropic) 
		{
			float MaxAnisotropy;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &MaxAnisotropy);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, MaxAnisotropy);
		}
		else
		{
			cerr << "WARNING: Anisotroy filtering is not supported. Using Nearest_Linear" << endl;
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
	}
	else if (TextureFilterType == ETextureFilterType::Nearest_Linear)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else if (TextureFilterType == ETextureFilterType::Nearest_Nearest)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
}
